#!/bin/bash

ENV=$1

## FRONT ##
cd /home/ubuntu/workspace/rest-$ENV/rest-front;
git pull;
. ~/.nvm/nvm.sh
. ~/.profile
. ~/.bashrc
nvm use 14.15;
npm install;
ng build --prod;

## BACK ##
cd /home/ubuntu/workspace/rest-$ENV/rest-api;
git pull;
. ~/.nvm/nvm.sh
. ~/.profile
. ~/.bashrc
nvm use 10.18;
npm install;

cd /home/ubuntu/workspace/rest-$ENV/rest-front;
sudo cp -r dist/* /var/www/$ENV/html;